# Aden Mengistie
# CMPE 102 - HW: Python - ASCII

def uppercase (letter):
    return chr(ord(letter) + (ord('A') - ord('a')))
def lowercase (letter):
    return chr(ord(letter) - (ord('A') - ord('a')))
def isAlphabet (element):
    uppercase = ord(element) - ord('A')
    lowercase = ord(element) - ord('a')
    return bool ((uppercase >= 0 and uppercase <= 26) or (lowercase >= 0 and lowercase <= 26))
def isDigit (element):
    digit = ord(element) - ord('0')
    return bool (digit >= 0 and digit <= 9)
def isSpecialCharacter (element):
    return bool ((not(isDigit(element))) and (not(isAlphabet(element))))
    
val = input("Please enter lowercase letter: ")
print (uppercase(val))
val = input("Please enter uppercase letter: ")
print (lowercase(val))
val = input("Please enter element: ")
print ("This is an alphabet: " + str(isAlphabet(val)))
print ("This is a digit: " + str(isDigit(val)))
print ("This is an special character: " + str(isSpecialCharacter(val)))




    